L'objectif de ce projet est de comparer les performances entre le framework de test intégré à WinDev et [wxUnit](https://gitlab.com/Johjo/wxunit/)

Il y a 10 fichiers de test **WinDev** (fichiers wxt) et 10 fichiers de test **wxUnit** (classes (fichiers wdc) héritant de cTestCase et préfixées par tu (pour test unit)). 

* Pour lancer les tests **WinDev**, il suffit de cliquer sur le bouton *Lancer tous les tests du projet*.
* Pour lancer les tests **wxUnit**, il suffit de faire un go du projet.

Concernant wxUnit, il faut au préalable avoir installé et paramétré [wxUnitViewer](https://gitlab.com/Johjo/wxunitviewer).




